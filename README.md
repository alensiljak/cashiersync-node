# cashiersync-node

Cashier Sync implemented with Node.js

It is a replacement for the Python version of CashierSync.

## Development

Using the dev version. Link the package with `yarn link` from the root folder, and unlink with `yarn unlink`.
[Instructions](https://yarnpkg.com/lang/en/docs/cli/link/).

## Publishing

Run

`npm publish`

from the directory that contains the package.json.
It is necessary to log in beforehand, using `npm login`.

[Ref](https://zellwk.com/blog/publish-to-npm/)

## Running

- install nodejs
- install yarn
- `yarn global add cashiersync`
- `cashiersync`

Keep the package up-to-date with `yarn global upgrade`.

If running in Termux, add the termux-exec (`pkg install termux-exec`) package in order to run Node scripts.
If this does not work, create a script in ~/.shortcuts with the content:
```
#! /bin/bash
node /data/data/com.termux/files/usr/bin/cashiersync
```
