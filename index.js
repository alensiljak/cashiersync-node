#! /usr/bin/env node
/*
    Cashiersync server.
    Was Server demo, now is a working example.
*/
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');

var app = express(),
  port = process.env.PORT || 5000;

// middleware
app.use(morgan('tiny'));

// routes
app.get('/', cors(), (req, res) => {
    res.status(200).send("Hello World!");
});

app.get('/api/v1/todos', (req, res) => {
    res.status(200).send({
      success: 'true',
      message: 'todos retrieved successfully',
      todos: 'some todos here'
    })
  });  

// test route
var runSomething = function() {
    const util = require('util');
    const { exec } = require('child_process');
    exec('ledger --version', (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            return;
        }

        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);

        return stdout;
    });
};

async function ls() {
    const exec = util.promisify(require('child_process').exec);

    const { stdout, stderr } = await exec('ls');
    console.log('stdout:', stdout);
    console.log('stderr:', stderr);
  }

var runSync = function(cmdLine) {
    const { execSync } = require('child_process');
    let stdout = execSync(cmdLine, { 
        encoding: 'utf8'
        // maxBuffer: 50 * 1024 * 1024,
        //stdio: 'inherit' 
    });
    return stdout;
}

app.get('/run', (req, res) => {
    // run command
    //var output = runSomething();
    // var output = ls();
    var output = runSync('ledger --version');
    //.toString();

    res.status(200).send({
        "result": "result: " + output
    });
});

var ledger = function(parameters) {
    var cmd = 'ledger ' + parameters;
    var result = runSync(cmd);
    return result;
};

// Real route(s)
app.get('/accounts', cors(), (req, res) => {
    var params = "accounts";
    // run ledger with the parameter
    var output = ledger(params);
    //console.log(output);
    //res.status(200).send({output});
    res.send(output);
});

app.get('/balance', cors(), (req, res) => {
    var params = "b --flat --no-total";
    var output = ledger(params);
    // console.log(output);
    //res.status(200).send({
    res.send(output);
    //});
});
app.get('/currentValues', cors(), (req, res) => {
    // console.log(req);
    var root = req.query.root;
    var currency = req.query.currency;

    var params = `b ^${root} -X ${currency} --flat --no-total`;
    var output = ledger(params);
    // console.log(output);
    // res.status(200).send({
    //     output
    // });
    res.send(output);
});

// run.
app.listen(port, () => {
    console.log(`Cashiersync RESTful API server started on: ${port}`);
});

